angular.module('bike.controllers', [])

.controller('BikesCtrl', function($scope, BikeService) {


	$scope.bikes = BikeService.load();

    
})

.controller('MapCtrl', function($scope, BikeService) {

$scope.coords = { latitude: 53.353462, longitude: -6.265305};

	$scope.bikeStations = BikeService.load();

    
})



.controller('BikeDetailCtrl', function($scope, $stateParams, BikeService) {
  console.log("Getting param ",$stateParams.bikeId )
  $scope.bikeitem = BikeService.get($stateParams.bikeId);

  var myBike = BikeService.get($stateParams.bikeId)

  console.log("Found bike myBike", myBike);
  
  $scope.testing = "My test";
  
})