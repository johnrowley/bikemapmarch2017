
app.factory('BikeService', function ($q, $http) {

  var bikeListings = [];

  var bikeStation = {number: 15, address: "testing"};

  return {

    all: function () {

      return bikeListings;

    },

    get: function (station_id) {

      var deferred = $q.defer();
      var bikeServiceUrl = "https://api.jcdecaux.com/vls/v1/stations/" + station_id + "?contract=dublin&apiKey=";
      var key = "4a3b1b035a651d4b9c22b1adc7c291936ab4989c";

      //013c96fda1ac6937698c8402e42b0c31f2cc081e

return $http.get(bikeServiceUrl + key, {}).then(function ( response) {

//console.log("response" , response.data)
//bikeStation = response.data;
return bikeStation;

})

/*
      $http.get(bikeServiceUrl + key, {}).success(function (data) {


        //  console.log("loading", bikeServiceUrl + key, data);

        console.log(data);

        bikeStation = data;

        console.log("Station is" + data.number);


        //resolve the deferred promise so data is ultimatley sent back to caller
      



      }).error(function () {

        alert('there was an error loading station');

      });
  deferred.resolve();
      return bikeStation;
*/
    },

    load: function () {
      //Set up angular promiseand defer
      var deferred = $q.defer();

      var serviceUrl = "https://api.jcdecaux.com/vls/v1/stations?contract=dublin&apiKey=";
      var key = "013c96fda1ac6937698c8402e42b0c31f2cc081e";



      $http.get(serviceUrl + key, {}).success(function (data) {


        console.log("loading", serviceUrl + key, data);

        angular.forEach(data, function (bikeItem) {

          //console.log(bikeItem)

          bikeListings.push(bikeItem);


        });

        //resolve the deferred promise so data is ultimately sent back to caller
        deferred.resolve();

      }).error(function () {

        alert('there was an error');

      });


      return bikeListings;

    }



  }







});